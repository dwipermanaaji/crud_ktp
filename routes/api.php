<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('users', 'API\UserController@details');
    Route::get('ktp', 'API\KtpController@index');
    Route::post('ktp', 'API\KtpController@store');
    Route::get('ktp/{id}', 'API\KtpController@show');
    Route::put('ktp/{id}', 'API\KtpController@update');
    Route::delete('ktp/{id}', 'API\KtpController@destroy');


});
