@extends('layouts.app');

@section('content')
    <div class="row">
        <div class="col-12 mb-2 d-flex flex-row-reverse">
            <a href="{{route('ktp.create')}}" class="btn btn-primary">Tambah Data</a>
        </div>
        <div class="col-12">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">NIK</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Tempat/Tanggal Lahir</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$item->nik}}</td>
                                <td>{{$item->nama}}</td>
                                <td>{{$item->tempat_lahir.', '.$item->tanggal_lahir}}</td>
                                <td>{{$item->jenis_kelamin}}</td>
                                <td>
                                    <form action="{{route('ktp.destroy',$item->id)}}" method="POST" onsubmit="return ConfirmDelete()">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{route('ktp.edit',$item->id)}}" class="btn btn-link">Edit</a>
                                        <button class="btn btn-link">
                                            Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </table>
        </div>
    </div>
@endsection
