@extends('layouts.app');

@section('content')
    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <a href="{{route('ktp.index')}}">Kembali</a>
        </div>
        <div class="col-12">
            <form class="row" method="POST" action="{{route('ktp.store')}}">
                @csrf
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">NIK</label>
                        {!! Form::text('nik', null, ['class'=>'form-control', 'placeholder'=>"Masukan NIK..."]) !!}
                        {!! $errors->first('nik', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Nama</label>
                        {!! Form::text('nama', null, ['class'=>'form-control', 'placeholder'=>"Masukan Nama..."]) !!}
                        {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Tempat Lahir</label>
                        {!! Form::text('tempat_lahir', null, ['class'=>'form-control', 'placeholder'=>"Masukan Tempat Lahir..."]) !!}
                        {!! $errors->first('tempat_lahir', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Tanggal Lahir</label>
                        {!! Form::date('tanggal_lahir', null, ['class'=>'form-control', 'placeholder'=>"Masukan tanggal_lahir..."]) !!}
                        {!! $errors->first('tanggal_lahir', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Jenis Kelamin</label>
                        {!! Form::select('jenis_kelamin', ['L'=>'Laki-Laki','P'=>'Perempuan'], null, ['class'=>'form-control','placeholder'=>'Pilih Jenis Kelamin']) !!}
                        {!! $errors->first('jenis_kelamin', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Alamat</label>
                        {!! Form::textarea('alamat', null, ['class'=>'form-control', 'placeholder'=>"Masukan Alamat...",'rows'=>'3']) !!}
                        {!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">RT</label>
                        {!! Form::text('rt', null, ['class'=>'form-control', 'placeholder'=>"Masukan rt..."]) !!}
                        {!! $errors->first('rt', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">RW</label>
                        {!! Form::text('rw', null, ['class'=>'form-control', 'placeholder'=>"Masukan RW..."]) !!}
                        {!! $errors->first('rw', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Desa</label>
                        {!! Form::text('desa', null, ['class'=>'form-control', 'placeholder'=>"Masukan Desa..."]) !!}
                        {!! $errors->first('desa', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Kecamatan</label>
                        {!! Form::text('kecamatan', null, ['class'=>'form-control', 'placeholder'=>"Masukan Kecamatan..."]) !!}
                        {!! $errors->first('kecamatan', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Agama</label>
                        {!! Form::select('agama', ['Islam'=>'Islam','Kristen Protestan'=>'Kristen Protestan','Katholik'=>'Katholik','Budha'=>'Budha','Konghucu'=>'Konghucu'], null, ['class'=>'form-control','placeholder'=>'Pilih Agama']) !!}
                        {!! $errors->first('agama', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Status Perkawinan</label>
                        {!! Form::select('status_perkawinan', ['Belum Nikah'=>'Belum Nikah','Menikah'=>'Menikah','Duda'=>'Duda','Janda'=>'Janda'], null, ['class'=>'form-control','placeholder'=>'Pilih Status Perkawinan']) !!}
                        {!! $errors->first('status_perkawinan', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Pekerjaan</label>
                        {!! Form::text('pekerjaan', null, ['class'=>'form-control', 'placeholder'=>"Masukan Pekerjaan..."]) !!}
                        {!! $errors->first('pekerjaan', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Kewarganegaraan</label>
                        {!! Form::select('kewarganegaraan', ['WNI'=>'WNI','WNA'=>'WNA'], null, ['class'=>'form-control','placeholder'=>'Pilih Kewarganegaraan']) !!}
                        {!! $errors->first('kewarganegaraan', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="for">Berlaku Hingga</label>
                        {!! Form::date('berlaku_hingga', null, ['class'=>'form-control', 'placeholder'=>"Masukan Berlaku Hingga..."]) !!}
                        {!! $errors->first('berlaku_hingga', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
@endsection
