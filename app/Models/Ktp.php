<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ktp extends Model
{
    protected $table = 'ktp';

    protected $fillable = [
            'nik',
            'nama',
            'tempat_lahir',
            'tanggal_lahir',
            'jenis_kelamin',
            'alamat',
            'rt',
            'rw',
            'desa',
            'kecamatan',
            'agama',
            'status_perkawinan',
            'pekerjaan',
            'kewarganegaraan',
            'berlaku_hingga',
    ];
}
