<?php

namespace App\Http\Controllers;

use App\Models\Ktp;
use Illuminate\Http\Request;


class KtpController extends Controller
{

    public function index()
    {
        $data = Ktp::get();
        return view('ktp.index', compact('data'));
    }

    public function create()
    {
        return view('ktp.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'nik' => 'required',
            'nama' => 'required',
            'tempat_lahir'=> 'required',
            'tanggal_lahir'=> 'required',
            'jenis_kelamin'=> 'required',
            'alamat'=> 'required',
            'rt'=> 'required',
            'rw'=> 'required',
            'desa'=> 'required',
            'kecamatan'=> 'required',
            'agama'=> 'required',
            'status_perkawinan'=> 'required',
            'pekerjaan'=> 'required',
            'kewarganegaraan'=> 'required',
            'berlaku_hingga'=> 'nullable',
        ]);

        $requestData = $request->all();
        $store = KTP::create($requestData);
        return redirect(Route('ktp.index'))->with(['success' => 'KTP: ' . $store->name . ' ditambahkan']);
    }

    public function show($id)
    {

    }


    public function edit($id)
    {
        $edit = Ktp::find($id);
        return view('ktp.edit',compact('edit'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nik' => 'required|max:12',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'desa' => 'required',
            'kecamatan' => 'required',
            'agama' => 'required',
            'status_perkawinan' => 'required',
            'pekerjaan' => 'required',
            'kewarganegaraan' => 'required',
            'berlaku_hingga' => 'nullable',
        ]);
        $update = Ktp::find($id);
        $requestData = $request->all();
        $update->update($requestData);
        return redirect(Route('ktp.index'))->with(['success' => 'KTP: ' . $update->name . ' diperbaharui']);
    }


    public function destroy($id)
    {
        $destroy = Ktp::find($id);
        $destroy->delete();
        return redirect(Route('ktp.index'))->with(['success' => 'KTP: ' . $destroy->name . ' dihapus']);
    }
}
