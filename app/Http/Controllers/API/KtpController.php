<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\KtpCollection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ktp;

class KtpController extends Controller
{
    public $successStatus = 200;

    public function index()
    {
        $data = Ktp::orderBy('created_at','desc');
        if (request()->q != '') {
            $data = $data->where('nik', 'LIKE', '%' . request()->q . '%');
            $data = $data->orWhere('nama', 'LIKE', '%' . request()->q . '%');
        }
        $data = $data->paginate(10);
        return new KtpCollection($data);
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'nik' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'desa' => 'required',
            'kecamatan' => 'required',
            'agama' => 'required',
            'status_perkawinan' => 'required',
            'pekerjaan' => 'required',
            'kewarganegaraan' => 'required',
            'berlaku_hingga' => 'nullable',
        ]);

        try {
            $requestData = $request->all();
            $store = KTP::create($requestData);
            return response()->json(['status' => 'success','data'=>$store]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed']);
        }
    }


    public function show($id)
    {
        try {
            $data = Ktp::findOrFail($id);
            return response()->json(['status' => 'success','data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed']);
        }
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nik' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'desa' => 'required',
            'kecamatan' => 'required',
            'agama' => 'required',
            'status_perkawinan' => 'required',
            'pekerjaan' => 'required',
            'kewarganegaraan' => 'required',
            'berlaku_hingga' => 'nullable',
        ]);

        try {
            $requestData = $request->all();
            $data = Ktp::findOrFail($id);
            $data->update($requestData);
            return response()->json(['status' => 'success','data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed']);
        }

    }

    public function destroy($id)
    {
        try {
            $destroy = Ktp::findOrFail($id);
            $destroy->delete();
            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed']);
        }
    }
}
